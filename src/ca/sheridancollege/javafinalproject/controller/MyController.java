package ca.sheridancollege.javafinalproject.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ca.sheridancollege.javafinalproject.beans.JobApplicant;
import ca.sheridancollege.javafinalproject.beans.JobOpportunity;
import ca.sheridancollege.javafinalproject.dao.DAO;

@Controller
public class MyController {

	DAO dao = new DAO();

	@GetMapping("/")
	public String index(Model model) {
		return "index";
	}

	@GetMapping("/addJob")
	public String addJob(Model model) {
		JobOpportunity jobOpportunity = new JobOpportunity();

		model.addAttribute("jobOpportunity", jobOpportunity);

		return "addJob";
	}

	@GetMapping("/second")
	public String info(Model model) {
		model.addAttribute("appl", dao.getAllApplicants());
		return "second";
	}

	@RequestMapping(value = "/saveJob", method = RequestMethod.POST)
	public String saveJob(Model model, @ModelAttribute JobOpportunity jobOpportunity) {
		dao.saveJob(jobOpportunity);
		
		model.addAttribute("jobList", dao.displayJobOpportunity());
		
		return "index";
	}
	
	@GetMapping("/addApplicant")
	public String applicant(Model model) {
		JobApplicant jobApplicant = new JobApplicant();
		model.addAttribute("jobApplicant", jobApplicant);
		return "addApplicant";
	}

	@PostMapping("/saveApplicant")
	public String saveApplicant(Model model, @ModelAttribute @Valid JobApplicant ja, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "addApplicant";
		}else {
			dao.saveApplicant(ja);
			return "index";
		}
	}

}
