package ca.sheridancollege.javafinalproject.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import ca.sheridancollege.javafinalproject.beans.JobApplicant;
import ca.sheridancollege.javafinalproject.beans.JobOpportunity;

public class DAO {
	SessionFactory sessionFactory = new Configuration()
			.configure("ca/sheridancollege/javafinalproject/config/hibernate.cfg.xml")
			.buildSessionFactory();
	
	public JobApplicant getApplicantById(int id) {
		Session session = sessionFactory.openSession(); 
		session.beginTransaction();
		
		JobApplicant temp = session.get(JobApplicant.class, id);
		
		session.getTransaction().commit();
		session.close();
		
		return temp;
	}
	
	public List<JobApplicant> getAllApplicants() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query = session.createNamedQuery("JobApplicant.getList");
		List<JobApplicant> jblist = query.getResultList();
		
		session.getTransaction().commit();
		session.close();
		return jblist;
	}
	
	public void saveJob(JobOpportunity jobOpportunity) {
	
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.saveOrUpdate(jobOpportunity);
		session.getTransaction().commit();
		session.close();
	
	}
	
	public void saveApplicant(JobApplicant ja) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.saveOrUpdate(ja);
		session.getTransaction().commit();
		session.close();
	}

	
	public List<JobOpportunity> displayJobOpportunity() {
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query= session.createNamedQuery("Job.getJobList");
		List<JobOpportunity> jobList = query.getResultList();

		session.getTransaction().commit();
		session.close();
		
		return jobList;
		
	}
}
