package ca.sheridancollege.javafinalproject.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Company {
	@GeneratedValue
	@Id
	private int id;
	private String companyName;
	private String companyAddress;
	private String companyDescription;
	
}
