package ca.sheridancollege.javafinalproject.beans;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@NamedQuery(name = "JobApplicant.getList", query = "from JobApplicant")
public class JobApplicant implements Serializable{
	@GeneratedValue
	@Id
	private int id;
	private String password;
	@NotNull
	@Email
	private String emailAddress;
	@NotNull
	@Size(min=2, max=40)
	private String name;
	private String address;
	@Pattern(regexp = "^(1\\s|1|)?((\\(\\d{3}\\))|\\d{3})(\\-|\\s)?(\\d{3})(\\-|\\s)?(\\d{4})$", message="Must be a"
			+ " telephone number in the form of 555-555-5555, (555)555-5555, (555) 555-5555, 555 555 5555," + 
			" 5555555555, or 1 555 555 5555.")
	private String phoneNum;	
	private String honorific;
	private Boolean resumeChecked = false;
	
	public Boolean hasResume() 
	{
		return true;
	}
}
