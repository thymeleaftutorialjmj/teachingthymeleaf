package ca.sheridancollege.javafinalproject.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@NamedQuery(name="Job.getJobList", query="from JobOpportunity")
public class JobOpportunity {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private enum EmploymentType {
		FULLTIME,
		PARTTIME,
		CONTRACT,
		TEMPORARY,
		SEASONAL,
		INTERNSHIP
	};

	private String jobTitle;
	
	private String employer;
	private String companyAddress;
	private String contactName;
	private String contactPhone;
	
	@Length(min=1, message="* Info required")
	@Column(columnDefinition="TEXT")
	private String jobDescription;
	private EmploymentType type;
	private int annualSalary;
	private int hourlyRate;

}
